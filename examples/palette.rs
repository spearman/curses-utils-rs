extern crate bmp;
extern crate log;
extern crate mat;
extern crate pancurses;
extern crate easycurses;
extern crate env_logger;

extern crate curses_utils;

use curses_utils::*;

const LOG_LEVEL    : log::LevelFilter = log::LevelFilter::Trace;
const LOG_FILENAME : &'static str     = "palette.log";

fn main() {
  use easycurses::{Color, ColorPair, Input};
  println!("palette: main...");
  { // log init
    let log_file = Box::new (std::fs::File::create (LOG_FILENAME).unwrap());
    env_logger::Builder::new()
      .filter_level (LOG_LEVEL)
      .parse_default_env()
      .target (env_logger::Target::Pipe (log_file))
      .init();
    println!("logging output to {:?}", LOG_FILENAME);
    log::info!("example log begin");
  }

  // init curses
  let mut curses = Curses::default();
  curses.log_info();
  // draw function
  let mut iter : u64 = 0;
  let draw = |curses : &mut Curses, iter : &mut u64, clear_color : ColorPair| {
    use pancurses::chtype;
    curses.set_color_pair (clear_color);
    let (attrs, colors) = curses.win().attrget();
    log::trace!("window colors: {:016b}", colors);
    curses.win().bkgdset (b' ' as chtype | color_pair (colors as chtype));
    curses.win().erase();
    //curses.clear();

    let ch = pancurses::ACS_CKBOARD();
    let dim = pancurses::A_DIM;
    let top  = 3;
    let mut left = 4;
    let mut row = top;
    let mut col = left;

    let draw_color = |color, row : &mut i32, col : &mut i32, left : i32| {
      let color_pair = color_pair_attr (Color::Black, color);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (Color::Black, color);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *col += 1;
      let color_pair = color_pair_attr (color, color);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (color, color);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *col += 1;
      let color_pair = color_pair_attr (Color::White, color);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (Color::White, color);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *row += 1;
      *col = left;

      let color_pair = color_pair_attr (color, Color::Black);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (color, Color::Black);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *col += 1;
      let color_pair = color_pair_attr (color, color);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (color, color);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *col += 1;
      let color_pair = color_pair_attr (color, Color::White);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (color, Color::White);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *row += 1;
      *col = left;
    };

    let draw_color_mixed =
      |color1, color2, row : &mut i32, col : &mut i32, left : i32|
    {
      let color_pair = color_pair_attr (color1, color2);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (color1, color2);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *row += 1;
      *col = left;
      let color_pair = color_pair_attr (color2, color1);
      curses.win().mvaddch (*row, *col, ch | color_pair | dim);
      *col += 1;
      let color_pair = color_pair_attr (color2, color1);
      curses.win().mvaddch (*row, *col, ch | color_pair);
      *row += 1;
      *col = left;
    };

    let color = color_pair_attr (Color::Black, Color::Black);
    curses.win().mvaddch (row, col, ch | color | dim);
    col += 1;
    let color = color_pair_attr (Color::Black, Color::Black);
    curses.win().mvaddch (row, col, ch | color);
    col += 1;
    let color = color_pair_attr (Color::White, Color::Black);
    curses.win().mvaddch (row, col, ch | color | dim);
    col += 1;
    let color = color_pair_attr (Color::White, Color::Black);
    curses.win().mvaddch (row, col, ch | color);
    col += 1;
    let color = color_pair_attr (Color::White, Color::White);
    curses.win().mvaddch (row, col, ch | color | dim);
    col += 1;
    let color = color_pair_attr (Color::White, Color::White);
    curses.win().mvaddch (row, col, ch | color);
    row += 1;
    col = left;

    let color = color_pair_attr (Color::Black, Color::Black);
    curses.win().mvaddch (row, col, ch | color | dim);
    col += 1;
    let color = color_pair_attr (Color::Black, Color::Black);
    curses.win().mvaddch (row, col, ch | color);
    col += 1;
    let color = color_pair_attr (Color::Black, Color::White);
    curses.win().mvaddch (row, col, ch | color | dim);
    col += 1;
    let color = color_pair_attr (Color::Black, Color::White);
    curses.win().mvaddch (row, col, ch | color);
    col += 1;
    let color = color_pair_attr (Color::White, Color::White);
    curses.win().mvaddch (row, col, ch | color | dim);
    col += 1;
    let color = color_pair_attr (Color::White, Color::White);
    curses.win().mvaddch (row, col, ch | color);
    row += 1;
    col = left;

    draw_color (Color::Red, &mut row, &mut col, left);
    draw_color (Color::Green, &mut row, &mut col, left);
    draw_color (Color::Blue, &mut row, &mut col, left);
    draw_color (Color::Cyan, &mut row, &mut col, left);
    draw_color (Color::Magenta, &mut row, &mut col, left);
    draw_color (Color::Yellow, &mut row, &mut col, left);

    draw_color_mixed (Color::Red, Color::Magenta, &mut row, &mut col, left);
    draw_color_mixed (Color::Red, Color::Yellow, &mut row, &mut col, left);
    draw_color_mixed (Color::Green, Color::Yellow, &mut row, &mut col, left);
    draw_color_mixed (Color::Green, Color::Cyan, &mut row, &mut col, left);
    draw_color_mixed (Color::Blue, Color::Cyan, &mut row, &mut col, left);
    draw_color_mixed (Color::Blue, Color::Magenta, &mut row, &mut col, left);

    left += 2;
    row -= 12;

    draw_color_mixed (Color::Red, Color::Cyan, &mut row, &mut col, left);
    draw_color_mixed (Color::Red, Color::Green, &mut row, &mut col, left);
    draw_color_mixed (Color::Green, Color::Magenta, &mut row, &mut col, left);
    draw_color_mixed (Color::Green, Color::Red, &mut row, &mut col, left);
    draw_color_mixed (Color::Blue, Color::Yellow, &mut row, &mut col, left);
    draw_color_mixed (Color::Blue, Color::Red, &mut row, &mut col, left);


    curses.win().attrset (attrs);

    assert!(curses.refresh());
    *iter += 1;
  };
  let mut clear_color = ColorPair::new (Color::White, Color::Black);
  draw (&mut curses, &mut iter, clear_color);

  // main loop
  while let Some (input) = curses.getch_wait() {
    log::debug!("input: {:?}", input);
    let (_attrs, _color) = curses.win().attrget();
    match input {
      Input::Character ('b') | Input::Character ('B') => {
        clear_color = ColorPair::new (Color::White, Color::Black);
      }
      Input::Character ('w') | Input::Character ('W') => {
        clear_color = ColorPair::new (Color::Black, Color::White);
      }
      Input::Character ('q') | Input::Character ('Q') => break,
      _ => {}
    }
    draw (&mut curses, &mut iter, clear_color);
  }

  log::info!("palette: ...log end");
  println!("palette: ...main");
}
