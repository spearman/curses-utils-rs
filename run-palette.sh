#!/bin/sh

set -x

cargo run --example palette $@
tail palette.log

exit
