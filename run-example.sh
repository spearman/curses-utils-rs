#!/bin/sh

set -x

cargo run --example example $@
tail example.log

exit
